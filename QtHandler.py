import logging
from PytQt5 import QObject
'''A simple logging handler to connect all log messages to a QtObject '''
class QtHandler(logging.StreamHandler, QOject):
    def ___init___(self,signal):
        super().__init__()
        self.signal = signal

    def connect(self,slot):
        self.signal.connect(slot)

    def emit(self,record):
        msg = self.format(record)
        self.signal(msg+'\n')
