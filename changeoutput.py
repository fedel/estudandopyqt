import logging
import exampleprogram
import sys
'''Example of how to change default stdout. This could be used to connect a script output to a pyqt widget '''

class myStream():
    def __init__(self):
        self.f =  open('teste.txt','a')

    def write(self,text):
        self.f.write(text)
    def flush(self):
        self.f.flush()

sys.stdout = myStream()
sys.stderr = myStream()

exampleprogram.run()
