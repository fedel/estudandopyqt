import logging
import exampleprogram
import sys
import threading
from PyQt5.QtCore import QObject, pyqtSignal
'''Example of how to change default stdout, and connect it to a widget from PyQT'''

class myStream(QObject):
    update = pyqtSignal(['QString'])
    def __init__(self,slot):
        super().__init__()
        self.update.connect(slot)
        self.f = open('debug','w')

    def write(self,text):
        self.update.emit(text)
        self.f.write(text)

    def flush(self):
        pass

def main(slot):
    stream = myStream(slot)
#    thr = threading.Thread(target = exampleprogram.run(stream))
#    thr.start()
    exampleprogram.run(stream)

