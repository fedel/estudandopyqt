import sys
from PyQt5.QtWidgets import QPushButton, QMessageBox, QApplication, QWidget

def window():
    app = QApplication(sys.argv)
    w = QWidget()
    b = QPushButton(w)
    b.setText("Show Message!")

    b.move(50,50)
    b.clicked.connect(showdialog)
    w.setWindowTitle("PyQt Dialog Example")
    w.show()
    sys.exit(app.exec_())

def showdialog():
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)

    msg.setText("Text")
    msg.setInformativeText("Infomative Text")
    msg.setWindowTitle("Window Title")
    msg.buttonClicked.connect(msgbtn)

    msg.exec_()

def msgbtn(val):
    print("returned")

if __name__ == '__main__':
    window()
