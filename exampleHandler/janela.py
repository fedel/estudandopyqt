import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton,\
    QLineEdit
from PyQt5.QtCore import pyqtSlot
import loghandler
import program
import threading

class myThread(threading.Thread):
    def __init__(self,handler):
        super().__init__()
        self.handler = handler

    def run(self):
        program.run(self.handler)


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 tests'
        self.left = 10
        self.top = 10
        self.width = 400
        self.height = 140

        #create the handler
        self.handler = loghandler.PyQtHandler()
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # Create textbox
        self.textbox = QLineEdit(self)
        self.textbox.move(20, 20)
        self.textbox.resize(280, 40)

        #connect handler signal to textbox(slot)
        self.handler.connect(self.textbox.setText)

        # Create a button in the window
        self.button = QPushButton('Show text', self)
        self.button.move(20, 80)

        # connect button to function on_click
#        self.button.clicked.connect(self.on_click)
        self.show()

        #call program/ without thread
        #program.run(self.handler)

        #call program/ with thread
        self.th = myThread(self.handler)
        self.th.start()



#    @pyqtSlot()
#    def on_click(self):
#        thread1 = recebedados.myThread(self.textbox.text(), self)
#        thread1.start()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
