import logging
import time

def run(handler = None):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    fmtLog = logging.Formatter('%(levelname)-8s: %(message)s')
    fh = logging.FileHandler('scaninfo.txt')
    fh.setLevel(logging.INFO)
    fh.setFormatter(fmtLog)
    logger.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(fmtLog)
    logger.addHandler(ch)
    
    #If there is a handler argument
    if handler != None:
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(fmtLog)
        logger.addHandler(handler)

    while True:
        time.sleep(1)
        logging.info("Mensagem 1")
        time.sleep(2)
        logging.info("Teste 2 ")



if __name__ == "__main__":
    run()
