import logging
from PyQt5.QtCore import QObject, pyqtSignal

class PyQtHandler(QObject,logging.StreamHandler):
    '''A simple handler to emit pyqt singnals'''
    signal = pyqtSignal(['QString'])

    def __init__(self):
        super().__init__()

    def connect(self,slot):
        self.signal.connect(slot)

    def emit(self,record):
        msg = self.format(record)
        self.signal.emit(msg)

