import logging

def run():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    fmtLog = logging.Formatter('%(levelname)-8s: %(message)s')
    fh = logging.FileHandler('scaninfo.txt')
    fh.setLevel(logging.INFO)
    fh.setFormatter(fmtLog)
    logger.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(fmtLog)
    logger.addHandler(ch)

    logging.info("Teste 1 ")
    logging.info("Teste 2 ")



if __name__ == "__main__":
    run()
