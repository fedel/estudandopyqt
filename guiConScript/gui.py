# -*- coding: utf-8 -*-
'''A simple gui that save and load parameters'''

from PyQt5 import QtCore, QtWidgets
import json


class Ui_MainWindow(object):
    configs = {}

    def clickSave(self):
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        self.saveConfigs(filename)

    def clickLoad(self):
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        self.loadConfigs(filename)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(545, 514)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(100, 10, 101, 27))
        self.comboBox.setEditable(False)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox_2 = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_2.setGeometry(QtCore.QRect(100, 50, 101, 27))
        self.comboBox_2.setObjectName("comboBox_2")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(100, 100, 181, 31))
        self.textEdit.setObjectName("textEdit")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 20, 81, 17))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 61, 17))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(20, 110, 61, 17))
        self.label_3.setObjectName("label_3")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(391, 10, 111, 27))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(401, 70, 101, 27))
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.pushButton.clicked.connect(self.clickSave)
        self.pushButton_2.clicked.connect(self.clickLoad)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.comboBox.setCurrentText(_translate("MainWindow", "3"))
        self.comboBox.setItemText(0, _translate("MainWindow", "3"))
        self.comboBox.setItemText(1, _translate("MainWindow", "2"))
        self.comboBox_2.setItemText(0, _translate("MainWindow", "motor1"))
        self.comboBox_2.setItemText(1, _translate("MainWindow", "motor2"))
        self.comboBox_2.setItemText(2, _translate("MainWindow", "motor3"))
        self.label.setText(_translate("MainWindow", "Dimensions"))
        self.label_2.setText(_translate("MainWindow", "FlyMotor"))
        self.label_3.setText(_translate("MainWindow", "CntTime"))
        self.pushButton.setText(_translate("MainWindow", "Save Settings"))
        self.pushButton_2.setText(_translate("MainWindow", "Load Settings"))

    def saveConfigs(self, filename):
        # TODO: find a better way to salve all information
        # (widgets inside another element?)
        self.configs.update({'comboBox': self.comboBox.currentIndex()})
        self.configs.update({'comboBox_2': self.comboBox_2.currentIndex()})
        self.configs.update({'textEdit': self.textEdit.toPlainText()})

        with open(filename, 'w') as f:
            json.dump(self.configs, f)

    def loadConfigs(self, filename):
        with open(filename, 'r') as f:
            self.configs = json.loads(f.read())

        # TODO: find a better way to do this
        for config, value in self.configs.items():
            if isinstance(getattr(self, config), QtWidgets.QComboBox):
                getattr(self, config).setCurrentIndex(value)
            else:
                getattr(self, config).setText(value)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
