import logging
class TesteHandler(logging.StreamHandler):
    def __init__(self):
        super().__init__()
        self.f = open('teste.txt','a')

    def emit(self,record):
        msg = self.format(record)
        self.f.write(msg+'\n')

