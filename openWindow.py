from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.uic import loadUi
from PyQt5 import QtCore
import sys

app = QApplication(sys.argv)
mainWindow = QMainWindow()
_translate = QtCore.QCoreApplication.translate


ui = loadUi('window.ui')
mainWindow.setCentralWidget(ui)
mainWindow.resize(725, 540)
mainWindow.setWindowTitle(_translate("MainWindow", "FlyScan - XRF"))
mainWindow.show()

sys.exit(app.exec_())
