'''A simple teste if pyqt5 is installed'''

try:
    from PyQt5.QtCore import pyqtSignal, pyqtSlot, pyqtProperty, QTimer
    pyqt5 = True
except ImportError:
    pyqt5 = False

if not pyqt5:
    from PyQt4.QtCore import pyqtSignal, pyqtSlot, pyqtProperty, QTimer

print(pyqt5)
