# encode=utf-8
import time
from PyQt5.QtCore import QObject, pyqtSignal
import threading


class geraDados(QObject):

    atualiza = pyqtSignal(['QString'])
    def __init__(self):
        super().__init__()

    def connect(self, func):
        self.atualiza.connect(func)


    def run(self):
        while True:
            #print("Executando...")
            self.atualiza.emit('iniciando')
            time.sleep(1)
            #print("Encerrado...")
            self.atualiza.emit('terminando')
            time.sleep(1)

#    def getAtualiza(self):
#        return self.atualiza

class recebeDados(QObject):

    def __init__(self):
        super().__init__()
        self.minhagd = geraDados()

#    def recebeDados(self):
#        self.minhagd = geraDados()
#        self.atualiza = self.minhagd.atualiza

#    def getAtualiza(self):
#        return self.atualiza

    def inicia(self,func):
        #cria thread geraDados
        self.minhagd.connect(func)
        self.minhathread = threading.Thread(target = self.minhagd.run)
        self.minhathread.start()
        #inicia thread geraDados
